Name:       qt5-qtwayland
Summary:    Qt Wayland
Version:    5.12.7
Release:    0
Group:      Qt/Qt
License:    LGPLv3
URL:        https://www.qt.io
Source0:    %{name}-%{version}.tar.bz2
Patch1:     qtwayland-fonconfig_case.patch
Patch2:     qtwayland-5.12.7-fix_qmax.patch
Patch3:     0002-Revert-most-of-Remove-QWaylandExtendedSurface-from-t.patch
Patch4:     0003-Fixup-QtKeyExtensionGlobal-export.patch

BuildRequires:  qt5-qmake >= 5.12.7
BuildRequires:  pkgconfig(Qt5Core) >= 5.12.7
BuildRequires:  pkgconfig(Qt5Qml) >= 5.12
BuildRequires:  pkgconfig(Qt5Quick) >= 5.12
BuildRequires:  pkgconfig(Qt5DBus) >= 5.12
BuildRequires:  pkgconfig(Qt5Gui) >= 5.12
BuildRequires:  pkgconfig(wayland-server) >= 1.6.0
BuildRequires:  pkgconfig(wayland-client) >= 1.6.0
BuildRequires:  pkgconfig(wayland-egl)
BuildRequires:  pkgconfig(egl)
BuildRequires:  pkgconfig(libudev)
BuildRequires:  pkgconfig(libinput)
BuildRequires:  qt5-qtgui-devel
BuildRequires:  pkgconfig(freetype2)
BuildRequires:  pkgconfig(fontconfig)

BuildRequires:  libxkbcommon-devel
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  libffi-devel
BuildRequires:  fdupes

Requires:       %{name}-client = %{version}-%{release}
Requires:       %{name}-compositor = %{version}-%{release}
Requires:       %{name}-import-compositor = %{version}-%{release}
Requires:       %{name}-plugin-platform-egl = %{version}-%{release}
Requires:       %{name}-plugin-platform-generic = %{version}-%{release}
Requires:       %{name}-plugin-graphics-integration-clients = %{version}-%{release}

Obsoletes:      %{name}-wayland_egl < 5.12.4
Provides:       %{name}-wayland_egl >= 5.12.4

%description
Qt is a cross-platform application and UI framework. Using Qt, you can
write web-enabled applications once and deploy them across desktop,
mobile and embedded systems without rewriting the source code.

This package contains the QtWayland client and compositor libraries

%package devel
Summary:        Development files for QtWayland
Group:          Qt/Qt
Requires:       %{name}-client-devel = %{version}-%{release}
Requires:       %{name}-compositor-devel = %{version}-%{release}

Obsoletes:      %{name}-wayland_egl-devel < 5.12.4
Provides:       %{name}-wayland_egl-devel >= 5.12.4

%description devel
Qt is a cross-platform application and UI framework. Using Qt, you can
write web-enabled applications once and deploy them across desktop,
mobile and embedded systems without rewriting the source code.

This package contains the Qt wayland development files.

%package client
Summary:            The QtWaylandClient library
Requires:           xkeyboard-config
Requires(post):     /sbin/ldconfig
Requires(postun):   /sbin/ldconfig

%description client
This package contains the QtWaylandClient library.

%package client-devel
Summary:    Development files for QtWaylandClient
Requires:   %{name}-client = %{version}-%{release}
Requires:   %{name}-tool-qtwaylandscanner = %{version}-%{release}
Requires:   pkgconfig(wayland-client) >= 1.6.0

%description client-devel
This package contains the files necessary to develop
applications that use QtWaylandClient

%package compositor
Summary:            The QtWaylandCompositor library
Requires:           xkeyboard-config
Requires(post):     /sbin/ldconfig
Requires(postun):   /sbin/ldconfig

%description compositor
This package contains the QtWaylandCompositor library.

%package compositor-devel
Summary:    Development files for QtWaylandCompositor
Requires:   %{name}-compositor = %{version}-%{release}
Requires:   %{name}-tool-qtwaylandscanner = %{version}-%{release}
Requires:   pkgconfig(wayland-server) >= 1.6.0

%description compositor-devel
This package contains the files necessary to develop
applications that use QtWaylandCompositor

%package tool-qtwaylandscanner
Summary:    The qtwaylandscanner development tool

%description tool-qtwaylandscanner
%{summary}.

%package import-compositor
Summary:    The QtWayland.Compositor QML import

%description import-compositor
%{summary}.

%package plugin-platform-egl
Summary:    The EGL QtWayland platform plugin

%description plugin-platform-egl
%{summary}.

%package plugin-platform-generic
Summary:    The generic QtWayland platform plugin

%description plugin-platform-generic
%{summary}.

%package plugin-graphics-integration-clients
Summary:    Clients graphics integration plugin for QtWayland

%description plugin-graphics-integration-clients
%{summary}.

%package plugin-graphics-integration-servers
Summary:    Servers graphics integration plugin for QtWayland

%description plugin-graphics-integration-servers
%{summary}.

%package plugin-shell-integration
Summary:    Shell integration plugin for QtWayland

%description plugin-shell-integration
%{summary}.


%prep
%setup -q -n %{name}-%{version}/upstream
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1

%build
export QTDIR=/usr/share/qt5
touch .git
%qmake5 
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
%qmake_install

# Fix wrong path in pkgconfig files
find %{buildroot}%{_libdir}/pkgconfig -type f -name '*.pc' \
-exec perl -pi -e "s, -L%{_builddir}/?\S+,,g" {} \;
# Fix wrong path in prl files
find %{buildroot}%{_libdir} -type f -name '*.prl' \
-exec sed -i -e "/^QMAKE_PRL_BUILD_DIR/d;s/\(QMAKE_PRL_LIBS =\).*/\1/" {} \;

# these manage to really royally screw up cmake
find %{buildroot}%{_libdir} -type f -name "*_*Plugin.cmake" \
-exec rm {} \;

%fdupes %{buildroot}/%{_includedir}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%post compositor -p /sbin/ldconfig
%postun compositor -p /sbin/ldconfig

%post client -p /sbin/ldconfig
%postun client -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%exclude %{_libdir}/qt5/plugins/wayland-shell-integration/libivi-shell.so
%exclude %{_libdir}/qt5/plugins/wayland-decoration-client/libbradient.so

%files devel
%defattr(-,root,root,-)
%exclude %{_libdir}/cmake/Qt5Gui

%files client
%defattr(-,root,root,-)
%{_libdir}/libQt5WaylandClient.so.*

%files client-devel
%{_includedir}/qt5/QtWaylandClient
%exclude %{_libdir}/libQt5WaylandClient.la
%{_libdir}/libQt5WaylandClient.so
%{_libdir}/libQt5WaylandClient.prl
%{_libdir}/pkgconfig/Qt5WaylandClient.pc
%{_libdir}/cmake/Qt5WaylandClient/*
%{_datadir}/qt5/mkspecs/modules/qt_lib_waylandclient.pri
%{_datadir}/qt5/mkspecs/modules/qt_lib_waylandclient_private.pri

%files compositor
%defattr(-,root,root,-)
%{_libdir}/libQt5WaylandCompositor.so.*

%files compositor-devel
%defattr(-,root,root,-)
%{_includedir}/qt5/QtWaylandCompositor
%exclude %{_libdir}/libQt5WaylandCompositor.la
%{_libdir}/libQt5WaylandCompositor.so
%{_libdir}/libQt5WaylandCompositor.prl
%{_libdir}/pkgconfig/Qt5WaylandCompositor.pc
%{_libdir}/cmake/Qt5WaylandCompositor/*
%{_datadir}/qt5/mkspecs/modules/qt_lib_waylandcompositor.pri
%{_datadir}/qt5/mkspecs/modules/qt_lib_waylandcompositor_private.pri

%files import-compositor
%defattr(-,root,root,-)
%{_libdir}/qt5/qml/QtWayland/Compositor

%files tool-qtwaylandscanner
%defattr(-,root,root,-)
%{_libdir}/qt5/bin/qtwaylandscanner

%files plugin-platform-egl
%defattr(-,root,root,-)
%{_libdir}/qt5/plugins/platforms/libqwayland-egl.so

%files plugin-platform-generic
%defattr(-,root,root,-)
%{_libdir}/qt5/plugins/platforms/libqwayland-generic.so

%files plugin-graphics-integration-clients
%defattr(-,root,root,-)
%{_libdir}/qt5/plugins/wayland-graphics-integration-client/*.so

%files plugin-graphics-integration-servers
%defattr(-,root,root,-)
%{_libdir}/qt5/plugins/wayland-graphics-integration-server/*.so

%files plugin-shell-integration
%defattr(-,root,root,-)
%{_libdir}/qt5/plugins/wayland-shell-integration/*.so
